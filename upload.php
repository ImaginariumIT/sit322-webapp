<?php
	require_once "vendor/autoload.php"; 

	use MicrosoftAzure\Storage\Blob\BlobRestProxy;
	use MicrosoftAzure\Storage\Queue\QueueRestProxy;
	use MicrosoftAzure\Storage\Common\ServiceException;
	use MicrosoftAzure\Storage\Blob\Models\CreateBlockBlobOptions;

	define('CHUNK_SIZE', 1024*1024);//Block Size = 1 MB

	if (!file_exists('uploads/')) {
		mkdir('uploads/', 0777, true);
	}
	$target_dir = "uploads/";
	$target_file = $target_dir . basename($_FILES["fileToUpload"]["name"]);
	$uploadOk = 1;
	$imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
	// Check if image file is a actual image or fake image
	if(isset($_POST["submit"])) {
	    if ($uploadOk == 0) {
			echo "Sorry, your file was not uploaded.";
			// if everything is ok, try to upload file
		} else {
			if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file)) {
			    //echo "The file ". basename( $_FILES["fileToUpload"]["name"]). " has been uploaded.";
			    try {
					$connectionString = "BlobEndpoint=https://caelumsit322217015925.blob.core.windows.net/;QueueEndpoint=https://caelumsit322217015925.queue.core.windows.net/;FileEndpoint=https://caelumsit322217015925.file.core.windows.net/;TableEndpoint=https://caelumsit322217015925.table.core.windows.net/;SharedAccessSignature=sv=2018-03-28&ss=b&srt=sco&sp=wc&se=2020-05-21T15:24:13Z&st=2019-05-21T07:24:13Z&spr=https&sig=kcvEcDmVJhDoNfcTHWV8qapGJmDxggQ2fQ4uAFFxkAc%3D";
					$blobClient = BlobRestProxy::createBlobService($connectionString);

					$content = fopen($target_file, "r");
					$myContainer = "uploaddata";
				    $blob_name = basename($_FILES["fileToUpload"]["name"]);
				    $blobClient->createBlockBlob($myContainer, $blob_name, $content);

				    //Add a queue message to let the correct function know to unzip the file we just uploaded.
				    $connectionString = "DefaultEndpointsProtocol=https;AccountName=caelumsit322217015925;AccountKey=dl2XSVc/U9xeK4EwUyJ5zvEje/OTYpIgWN+NnDCVbAr9FR2Vyijq47wW9olGMckKEI2KbElIEJI28Lh75Z0ibA==;EndpointSuffix=core.windows.net";
				    $queueClient = QueueRestProxy::createQueueService($connectionString);
				    $queueClient->createMessage("newuploadqueue", base64_encode(basename($_FILES["fileToUpload"]["name"])));
				    header("Location: ./index.html");

				} catch (ServiceException $e) {
			        $code = $e->getCode();
			        $error_message = $e->getMessage();
			        echo $code.": ".$error_message.PHP_EOL;
			    }
			} else {
			    echo "Sorry, there was an error uploading your file.";
			}
		}
	}

	
?>