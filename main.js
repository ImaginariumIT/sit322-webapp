function testConn()
{	
	$("#testConnResults").css("display", "inline-block");
	$("#testConnResults").text("Contacting...");
	
	$.ajax({
        url: "https://caelum20190521014219.azurewebsites.net/api/FTest?code=auRdamG3Vl5Tacz2AJcxFmT4abD8cEc2szskXKzugtgmPCXnqWVPLw==&name=Successful",
        type: "POST",
        processData: false,
        contentType: "application/json",
        success: (data, type, obj) => {
        	$("#testConnResults").text(data);
            console.log(data);
        },
        error: (data) => {
        	$("#testConnResults").text("Failed!");
            console.error(data);
        }
    });
}

function startMinimumForStation()
{
    $("#parameterName").val("stationNumber");
    $("#parameterValue").val("82039");
    $("#requestUrl").val("https://caelum20190521014219.azurewebsites.net/api/stationMinimum");
    $("#stationNumberPrompt").html("Station Number:");
    $("#stationNumberReturn").css("display", "none");
    $("#stationNumberReturn").text("...");
    console.log("startMinimumForStation()");
}

function startMaxMinimumForStation()
{
    $("#parameterName").val("stationNumber");
    $("#parameterValue").val("82039");
    $("#requestUrl").val("https://caelum20190521014219.azurewebsites.net/api/stationMaxMinimum");
    $("#stationNumberPrompt").html("Station Number:");
    $("#stationNumberReturn").css("display", "none");
    $("#stationNumberReturn").text("...");
    console.log("startMinimumForStation()");
}

function startMaximumForStation()
{
    $("#parameterName").val("stationNumber");
    $("#parameterValue").val("82039");
    $("#requestUrl").val("https://caelum20190521014219.azurewebsites.net/api/stationMaximum");
    $("#stationNumberPrompt").html("Station Number:");
    $("#stationNumberReturn").css("display", "none");
    $("#stationNumberReturn").text("...");
    console.log("startMaximumForStation()");
}

function startMinMaximumForStation()
{
    $("#parameterName").val("stationNumber");
    $("#parameterValue").val("82039");
    $("#requestUrl").val("https://caelum20190521014219.azurewebsites.net/api/stationMinMaximum");
    $("#stationNumberPrompt").html("Station Number:");
    $("#stationNumberReturn").css("display", "none");
    $("#stationNumberReturn").text("...");
    console.log("startMinMaximumForStation()");
}

function startTopTenLowsForStation()
{
    $("#tableparameterName").val("stationNumber");
    $("#tableparameterValue").val("82039");
    $("#tablerequestUrl").val("https://caelum20190521014219.azurewebsites.net/api/stationTopTenLows");
    $("#stationNumberTablePrompt").html("Station Number:");
    console.log("startTopTenLowsForStation()");
}

function startTopTenHighsForStation()
{
    $("#tableparameterName").val("stationNumber");
    $("#tableparameterValue").val("82039");
    $("#tablerequestUrl").val("https://caelum20190521014219.azurewebsites.net/api/stationTopTenHighs");
    $("#stationNumberTablePrompt").html("Station Number:");
    console.log("startTopTenHighsForStation()");
}

function startYearTrendForStation()
{
    $("#linegraphparameterName").val("stationNumber");
    $("#linegraphparameterValue").val("82039");
    $("#linegraphparameterName2").val("Year");
    $("#linegraphparameterValue2").val("1990");
    $("#linegraphrequestUrl").val("https://caelum20190521014219.azurewebsites.net/api/stationYearTrend");
    $("#stationLineGraphPrompt").html("Station Number and Year:");
    console.log("startYearTrendForStation()");
}

function sendRequestLineGraph()
{
    var parameterName = $("#linegraphparameterName").val();
    var parameterValue = $("#linegraphparameterValue").val();
    var parameterName2 = $("#linegraphparameterName2").val();
    var parameterValue2 = $("#linegraphparameterValue2").val();
    var requestUrl = $("#linegraphrequestUrl").val();

    var fullUrl = requestUrl + "?" + parameterName + "=" + parameterValue + "&" + parameterName2 + "=" + parameterValue2;
    console.log(fullUrl);

    $.ajax({
        url: fullUrl,
        type: "POST",
        processData: false,
        contentType: "application/json",
        success: (data, type, obj) => {
            var monthChunks = data.split("|");
            console.log(monthChunks);

            $("#lineGraph").remove();

            var newModal = $("<div id='lineGraph' class='modal' style='padding:0px !important; width: 700px;'></div>").appendTo('body');
            graph = document.getElementById('lineGraph');

            var minTrace = {
                x: ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sept','Oct','Nov','Dec'],
                y: [],
                mode: 'lines',
                name: 'Minimum avg',
                line:{
                    color: 'rgb(50, 50, 150)',
                    width: 3
                }
            }

            var midTrace = {
                x: ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sept','Oct','Nov','Dec'],
                y: [],
                mode: 'lines',
                name: 'Daily average avg',
                line:{
                    color: 'rgb(100, 100, 100)',
                    width: 1
                }
            }

            var maxTrace = {
                x: ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sept','Oct','Nov','Dec'],
                y: [],
                mode: 'lines',
                name: 'Maximum avg',
                line:{
                    color: 'rgb(150, 0, 0)',
                    width: 3
                }
            }

            for (var i = 0; i < 12; i++) {
                monthStats = monthChunks[i].split(",");
                minTrace.y[i] = monthStats[1];
                midTrace.y[i] = monthStats[2];
                maxTrace.y[i] = monthStats[3];
            }

            var data = [minTrace, midTrace, maxTrace];

            var layout = {
                title: 'Yearly trend',
                width: 500,
                height: 300,
                margin: 0,
                xaxis: {
                    title: 'Month'
                },
                yaxis: {
                    title: 'Temperature (°C)'
                }
            }

            Plotly.plot(graph, data, layout);

            newModal.modal();
        },
        error: (data) => {
            $("#table").text(data);
            console.error(data);
        }
    });
}

function sendRequestTable()
{
    var parameterName = $("#tableparameterName").val();
    var parameterValue = $("#tableparameterValue").val();
    var requestUrl = $("#tablerequestUrl").val();

    var fullUrl = requestUrl + "?" + parameterName + "=" + parameterValue;
    console.log(fullUrl);

    $.ajax({
        url: fullUrl,
        type: "POST",
        processData: false,
        contentType: "application/json",
        success: (data, type, obj) => {
            $("<div class='modal'>" + data + "</div>").appendTo('body').modal();
        },
        error: (data) => {
            $("#table").text(data);
            console.error(data);
        }
    });
}

function sendRequest()
{
    var parameterName = $("#parameterName").val();
    var parameterValue = $("#parameterValue").val();
    var requestUrl = $("#requestUrl").val();

    $("#stationNumberReturn").css("display", "inline-block");
    $("#stationNumberReturn").text("Running...");

    $.ajax({
        url: requestUrl + "?" + parameterName + "=" + parameterValue,
        type: "POST",
        processData: false,
        contentType: "application/json",
        success: (data, type, obj) => {
            $("#stationNumberReturn").text(data);
            console.log(data);
        },
        error: (data) => {
            $("#stationNumberReturn").text(data);
            console.error(data);
        }
    });
}

//Conn String
//BlobEndpoint=https://caelumsit322217015925.blob.core.windows.net/;QueueEndpoint=https://caelumsit322217015925.queue.core.windows.net/;FileEndpoint=https://caelumsit322217015925.file.core.windows.net/;TableEndpoint=https://caelumsit322217015925.table.core.windows.net/;SharedAccessSignature=sv=2018-03-28&ss=b&srt=sco&sp=w&se=2019-07-01T13:10:22Z&st=2019-05-21T05:10:22Z&spr=https&sig=GW1hBXQDpqPgX6VlGD4NwhDkxkbwGMAf%2FnyJWhkfOQA%3D
//SAS Token
//?sv=2018-03-28&ss=b&srt=sco&sp=w&se=2019-07-01T13:10:22Z&st=2019-05-21T05:10:22Z&spr=https&sig=GW1hBXQDpqPgX6VlGD4NwhDkxkbwGMAf%2FnyJWhkfOQA%3D
//Blob service SAS URL
//https://caelumsit322217015925.blob.core.windows.net/?sv=2018-03-28&ss=b&srt=sco&sp=w&se=2019-07-01T13:10:22Z&st=2019-05-21T05:10:22Z&spr=https&sig=GW1hBXQDpqPgX6VlGD4NwhDkxkbwGMAf%2FnyJWhkfOQA%3D
